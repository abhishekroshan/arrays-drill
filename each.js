function each(items, cb) {
  for (let index = 0; index < items.length; index++) {
    cb(items[index], index);
  }
}

// const items = [1, 2, 3, 4, 5, 5];

// each(items);
module.exports = each;
