function flatten(items) {
  let newArray = [];

  function helper(array) {
    for (let index = 0; index < array.length; index++) {
      if (!Array.isArray(array[index])) {
        newArray.push(array[index]);
        //if the value is not an array it should be an element
        //in that case we can just push the value in newArray
      } else {
        helper(array[index]);
        //if the value at index is an array we make a recursive call
        //to the helper function.
      }
    }
  }

  helper(items);

  return newArray;
}

module.exports = flatten;
