function filter(items, cb) {
  let filterArray = [];

  for (let index = 0; index < items.length; index++) {
    if (cb(items[index])) {
      //we pass the value into the cb function if it is truthy
      //then we can push the elements to the filter array.
      filterArray.push(items[index]);
    }
  }
  return filterArray;
}

module.exports = filter;
