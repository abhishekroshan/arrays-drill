function map(items, cb) {
  let newArray = [];
  for (let index = 0; index < items.length; index++) {
    newArray.push(cb(items[index]));
  }
  //the cb function takes the value as input and performs the
  //desired operation and then pushes the new modified value
  //into the newArray

  return newArray;
}

module.exports = map;
