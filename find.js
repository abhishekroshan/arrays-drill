function find(items, cb) {
  let result = undefined;

  for (let index = 0; index < items.length; index++) {
    if (cb(items[index])) {
      //we pass the value to the cb function if it return true
      //then we can return the value
      return (result = items[index]);
    }
  }

  return result;
}

module.exports = find;
