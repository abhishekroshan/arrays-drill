const find = require("../find");

function cb(value) {
  let testValue = 2;
  if (value === testValue) {
    return true;
  }
}
//the value which have to be searches should be entered
//in place of the '2'.

const items = [1, 2, 3, 4, 5, 5];

const result = find(items, cb);

console.log(result);
