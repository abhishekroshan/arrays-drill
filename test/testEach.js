const each = require("../each");

function cb(element, index) {
  console.log(`element at index ${index} is ${element}`);
}

const items = [1, 2, 3, 4, 5, 5];

each(items, cb);
