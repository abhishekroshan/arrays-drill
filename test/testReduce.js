const reduce = require("./reduce");

function cb(reducer, value) {
  return reducer + value;
}

const items = [1, 2, 3, 4, 5, 5];

const result = reduce(items, cb);

console.log(result);
