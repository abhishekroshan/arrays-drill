const filter = require("../filter");

const items = [1, 2, 3, 4, 5, 5, "a", "b", "d"];

function cb(value) {
  let condition = 3;
  //in place of condition we can change it to another condition
  //or we can also check the value with some property inside
  //the if-statement
  if (value > condition) {
    return true;
  } else {
    return false;
  }
}

const result = filter(items, cb);

console.log(result);
