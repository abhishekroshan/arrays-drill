const flatten = require("../flatten");

const items = [1, [2], [[3]], [[[4]]]];

const result = flatten(items);

console.log(result);
