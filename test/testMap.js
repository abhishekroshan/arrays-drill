const map = require("../map");

function cb(element) {
  return element * element;
}

const items = [1, 2, 3, 4, 5, 5];

console.log(map(items, cb));
