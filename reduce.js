function reduce(items, cb, startingValue) {
  let start = startingValue || 0;
  //if the starting vaalue is not provided we take it as 0 by default
  let reduced = items[start];
  //the reduced is the value in the items array at the index of starting point
  for (let index = start + 1; index < items.length; index++) {
    reduced = cb(reduced, items[index]);
    /*
    we iterate through the items array and the cb function adds the 
    value at the starting point and the value next to the starting point
    after that this value is assigned as the new reduced value
    */
  }
  return reduced;
}

module.exports = reduce;
